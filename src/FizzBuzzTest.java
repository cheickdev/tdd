import org.junit.*;
import static org.junit.Assert.*;

public class FizzBuzzTest {
	
	FizzBuzz instance;

	@Before
	public void setup() {
		instance = new FizzBuzz();
	}

	@Test
	public void shouldPrint_Fizz_WhenNumberIs_3() {
		assertEquals(FizzBuzz.FIZZ, instance.fizzBuzz(3));	
	}

	@Test
	public void shouldPrint_Buzz_WhenNumberIs_5() {
		assertEquals(FizzBuzz.BUZZ, instance.fizzBuzz(5));
	}

	@Test
	public void shouldPrint_FizzBuzz_WhenNumberIs_15() {
		assertEquals(FizzBuzz.FIZZ_BUZZ, instance.fizzBuzz(15));
	}

	@Test
	public void shouldPrint_7_WhenNumberIs_7() {
		assertEquals("7", instance.fizzBuzz(7));
	}

}
