import org.junit.*;
import static org.junit.Assert.*;
public class StringCalculatorTest {
	
	StringCalculator calculator;
	@Before
	public void setup() {
		calculator = new StringCalculator();
	}

	@Test
	public void shouldReturn_0_WhenNumbersIsEmpty() throws Exception {
		assertEquals(0, calculator.add(""));
	}

	@Test
	public void shouldReturn_1_WhenNumbersIs_1() throws Exception {
		assertEquals(1, calculator.add("1"));
	}

	@Test
	public void shouldReturn_6_WhenNumbersIs_1_DELIMITER_2_DELIMITER_3() throws Exception {
		assertEquals(6, calculator.add("1,2,3"));
	}

	@Test
	public void shouldReturn_6_WhenNumbersIs_1_NEW_LINE_2_DELIMITER_3() throws Exception  {
		assertEquals(6, calculator.add("1\n2,3"));
	}

	@Test
	public void shouldReturn_6_WhenFirstLineIs_DELIMITER_And_1_DELI_2_DELI_3() throws Exception {
		assertEquals(6, calculator.add(";\n1;2;3"));
	}

	@Test(expected = Exception.class)	
	public void shouldThrowExceptionWhenNumbersContainsNegativeValues() throws Exception {
		try {
			calculator.add("1,-2,3");
		} catch(Exception e) {
			assertEquals("negatives not allowed -2", e.getMessage());
			throw new Exception(e);
		}
	}
}
