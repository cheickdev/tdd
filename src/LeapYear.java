
public class LeapYear {
	private final int LEAP_YEAR_FIRST_DIVISOR = 4;
	private final int LEAP_YEAR_SECOND_DIVISOR = 100;
	private final int LEAP_YEAR_THIRD_DIVISOR = 400;
	public boolean isLeapYear(int year) {
		if(year % LEAP_YEAR_FIRST_DIVISOR != 0) {
			return false;
		}

		if(year % LEAP_YEAR_THIRD_DIVISOR == 0) {
			return true;
		}

		if(year % LEAP_YEAR_SECOND_DIVISOR == 0) {
			return false;
		}
		return true;
	}

	public static void main(String args[]) {
		LeapYear instance = new LeapYear();
		System.out.println(instance.isLeapYear(1996));
		System.out.println(instance.isLeapYear(1900));
		System.out.println(instance.isLeapYear(1906));
	}
}
