import org.junit.*;
import static org.junit.Assert.*;

import java.util.*;

public class PrimeFactorsTest {

	private List<Integer> list(int... args) {
		List<Integer> list = new ArrayList<>();
		for(int a : args) {
			list.add(a);
		}
		return list;
	}
	
	@Test
	public void shouldReturnEmptyListWhenNumberIsOne() {
		assertEquals(list(), PrimeFactors.generate(1));
	}

	@Test
	public void shouldReturn_2_WhenNumberIsTwo() {
		assertEquals(list(2), PrimeFactors.generate(2));
	}

	@Test
	public void shouldReturn_3_WhenNumberIsThree() {
		assertEquals(list(3), PrimeFactors.generate(3));
	}

	@Test
	public void shouldReturn_2_2_WhenNumberIsFour() {
		assertEquals(list(2, 2), PrimeFactors.generate(4));
	}

	@Test
	public void shouldReturn_2_3_WhenNumberIsSix() {
		assertEquals(list(2, 3), PrimeFactors.generate(6));
	}

	@Test
	public void shouldReturn_2_2_2_WhenNumberIsEight() {
		assertEquals(list(2, 2, 2), PrimeFactors.generate(8));
	}

	@Test
	public void shouldReturn_3_3_WhenNumberIsNine() {
		assertEquals(list(3, 3), PrimeFactors.generate(9));
	}

	@Test
	public void shouldReturn_2_3_7_WhenNumberIs_FortyFour() {
		assertEquals(list(2, 3, 7), PrimeFactors.generate(42));
	}
}
