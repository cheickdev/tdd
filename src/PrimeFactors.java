import java.util.*;
public class PrimeFactors {

	private static int[] computeMinimumDivisors(int n) {
		int[] divisors = new int[n+1];
		divisors[0] = 0;
		divisors[1] = 1;
		for(int i = 2; i <= n; i++) {
			for(int j = 1; j <= n / i; j++) {
				if(divisors[i * j] == 0) {
					divisors[i * j] = i;
				}
			}
		}
		return divisors;
	}

	public static List<Integer> generate(int n) {
		List<Integer> primes = new ArrayList<>();
		int[] divisors = computeMinimumDivisors(n);
		while(n > 1) {
			primes.add(divisors[n]);
			n /= divisors[n];
		}
		return primes;
	}

			/*int candidate = 2;
			while(n > 1) {
				while(n % candidate == 0) {
					primes.add(candidate);
					n /= candidate;
				}
				candidate++;
			}*/

}
