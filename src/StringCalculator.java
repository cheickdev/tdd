public class StringCalculator {

	private static final String POSITIVE_NUMBER_REGEX = "[0-9]+";
	private static final String DEFAULT_DELIMITER = ",";

	private String[] splitNumbersWithDelimiter(String delimiter, String numbers) {
		return numbers.split("[\n|"+delimiter+"]");
	}

	private String extractDelimiter(String numbers) {
		String delimiter = "";
		int indexOfNewLine = numbers.indexOf("\n");
		delimiter = numbers.substring(0, indexOfNewLine);
		return delimiter;
	}

	private String extractNumbersPart(String delimiter, String numbers) {
		return numbers.substring(delimiter.length() + 1);
	}

	private int computeNumberArraySum(String[] numbers) throws Exception {
		int s = 0;
		String negatives = "";
		for(String number: numbers) {
			if(number.startsWith("-")) {
				negatives += " " + number;
			} else {
				s += Integer.parseInt(number);
			}
		}
		if(!negatives.isEmpty()) {
			throw new Exception("negatives not allowed" + negatives);
		}
		return s;
	}

	private int computeSumWithDelimiter(String delimiter, String numbersPart) throws Exception {
		String[] numbersTab = splitNumbersWithDelimiter(delimiter, numbersPart);
		return computeNumberArraySum(numbersTab);	
	}

	private int addWithCustomDelimiter(String numbers)  throws Exception{
		String delimiter = extractDelimiter(numbers);
		String numbersPart = extractNumbersPart(delimiter, numbers);
		return computeSumWithDelimiter(delimiter, numbersPart);
	}

	private int addWithDefaultDelimiter(String numbers)  throws Exception{
		return computeSumWithDelimiter(DEFAULT_DELIMITER, numbers);
	}

	public int add(String numbers)  throws Exception{
		if(numbers.isEmpty()) {
			return 0;
		}
		if(!(numbers.charAt(0)+"").matches(POSITIVE_NUMBER_REGEX) ) {
			return addWithCustomDelimiter(numbers);
		}
		return addWithDefaultDelimiter(numbers);
	}
}
