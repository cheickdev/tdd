import org.junit.*;
import static org.junit.Assert.*;

public class LeapYearTest {
	LeapYear leapYear;
	@Before
	public void setup() {
		leapYear = new LeapYear();
	}

	@Test
	public void shouldReturnTrueWhenYearIs_1996() {
		assertTrue(leapYear.isLeapYear(1996));
	}

	@Test
	public void shouldReturnFalseWhenYearIs_1989() {
		assertFalse(leapYear.isLeapYear(1989));
	}
	
	@Test
	public void shouldReturnTrueWhenYearIs_2000() {
		assertTrue(leapYear.isLeapYear(2000));
	}

	@Test
	public void shouldReturnTrueWhenYearIs_2004() {
		assertTrue(leapYear.isLeapYear(2004));	
	}
}
