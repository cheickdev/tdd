public class FizzBuzz {
	public final static String FIZZ = "Fizz";
	public final static String BUZZ = "Buzz";
	public final static String FIZZ_BUZZ = "FizzBuzz";

	private boolean isNumberDivideThree(int number) {
		return number % 3 == 0;
	}

	private boolean isNumberDivideFive(int number) {
		return number % 5 == 0;
	}
	
	public String fizzBuzz(int number) {
		String output = "";
		if(isNumberDivideThree(number)) {
			output += FIZZ;
		}
		if(isNumberDivideFive(number)) {
			output += BUZZ;
		}
		if(output.isEmpty()) {
			output += number;
		}
		return output;
	}

	public String printFizzBuzz() {
		String result = fizzBuzz(1);
		for(int i = 2; i <= 100; i++)
			result += "," + fizzBuzz(i);
		return result;
	}

	public static void main(String[] args) {
		FizzBuzz fb = new FizzBuzz();
		System.out.println(fb.printFizzBuzz());
	}
}
